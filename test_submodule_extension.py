import os
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(current_dir, 'other'))

import my_module.my_submodule1
my_module.my_submodule1.foo1()

import my_module.my_submodule2
my_module.my_submodule2.foo2()
